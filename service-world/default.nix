{ pkgs ? import <nixpkgs> {} }:

with pkgs.stdenv;
with pkgs.lib;

pkgs.rustPlatform.buildRustPackage rec {
  name = "world-service";
  src  = pkgs.nix-gitignore.gitignoreSourcePure "target\n" ./.;

  cargoSha256 = "sha256:01fkr5gd0hagbmfn358hk4ap6iiz63nsg8758fi4p3inbcfndfmp";

  nativeBuildInputs = with pkgs; [ pkg-config makeWrapper ];
  buildInputs       = with pkgs; [ openssl ];
}
