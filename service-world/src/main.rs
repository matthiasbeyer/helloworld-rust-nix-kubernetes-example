use std::str::FromStr;

use anyhow::Context;
use actix_web::{get, App, HttpServer, Responder};

#[get("/")]
async fn world() -> impl Responder {
    "World"
}

#[actix_web::main]
async fn main() -> anyhow::Result<()> {
    env_logger::init_from_env(env_logger::Env::new().default_filter_or("info"));
    let bind = std::env::var("HOST").expect("environment: HOST variable not set");
    let port = std::env::var("PORT")
        .as_ref()
        .map(|p| u16::from_str(p))
        .expect("environment: PORT variable not set")
        .context("Failed to parse port as integer")?;

    HttpServer::new(|| App::new().service(world))
        .bind(format!("{}:{}", bind, port))?
        .run()
        .await
        .map_err(anyhow::Error::from)
}
