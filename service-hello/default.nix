{ pkgs ? import <nixpkgs> {} }:

with pkgs.stdenv;
with pkgs.lib;

pkgs.rustPlatform.buildRustPackage rec {
  name = "hello-service";
  src  = pkgs.nix-gitignore.gitignoreSourcePure "target\n" ./.;

  cargoSha256 = "sha256:0nam3yr99gg0lajdha0s9pijqwblgwggqi94mg7d3wz7vfhj8c31";

  nativeBuildInputs = with pkgs; [ pkg-config makeWrapper ];
  buildInputs       = with pkgs; [ openssl ];
}
