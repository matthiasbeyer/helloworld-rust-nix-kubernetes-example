{ type ? "dev" }:

let
  kubeVersion = "1.18";

  helloApp = rec {
    label = "hello";
    port  = 3000;

    cpu = if type == "dev"
    then "100m"
    else "1000m";

    imagePolicy = if type == "dev"
    then "Never"
    else "IfNotPresent";

    env = [
      { name = "APP_PORT"; value = "${toString port}"; }
      { name = "HOST"; value = "0.0.0.0"; }
      { name = "PORT"; value = "${toString port}"; }
    ];
  };

  worldApp = rec {
    label = "world";
    port  = 3000;

    cpu = if type == "dev"
    then "100m"
    else "1000m";

    imagePolicy = if type == "dev"
    then "Never"
    else "IfNotPresent";

    env = [
      { name = "APP_PORT"; value = "${toString port}"; }
      { name = "HOST"; value = "0.0.0.0"; }
      { name = "PORT"; value = "${toString port}"; }
    ];
  };

  joinerApp = rec {
    label = "joiner";
    port  = 3000;

    cpu = if type == "dev"
    then "100m"
    else "1000m";

    imagePolicy = if type == "dev"
    then "Never"
    else "IfNotPresent";

    env = [
      { name = "APP_PORT"; value = "${toString port}"; }
      { name = "HOST"; value = "0.0.0.0"; }
      { name = "PORT"; value = "${toString port}"; }
      { name = "HELLO_SERVICE"; value = "${helloApp.label}:${toString helloApp.port}"; }
      { name = "WORLD_SERVICE"; value = "${worldApp.label}:${toString worldApp.port}"; }
    ];
  };
in rec
{
  kubernetes.version = kubeVersion;

  kubernetes.resources.deployments = {
    "${helloApp.label}" = {
      metadata.labels.app = helloApp.label;

      spec = {
        replicas = 1;
        selector.matchLabels.app = helloApp.label;
        template = {
          metadata.labels.app = helloApp.label;
          spec.containers."${helloApp.label}" = {
            name = "${helloApp.label}";
            image = "hello-service:latest";
            imagePullPolicy = helloApp.imagePolicy;
            env = helloApp.env;
            resources.requests.cpu = helloApp.cpu;
            ports."${toString helloApp.port}" = {};
          };
        };
      };

    };

    "${worldApp.label}" = {
      metadata.labels.app = worldApp.label;

      spec = {
        replicas = 1;
        selector.matchLabels.app = worldApp.label;
        template = {
          metadata.labels.app = worldApp.label;
          spec.containers."${worldApp.label}" = {
            name = "${worldApp.label}";
            image = "world-service:latest";
            imagePullPolicy = worldApp.imagePolicy;
            env = worldApp.env;
            resources.requests.cpu = worldApp.cpu;
            ports."${toString worldApp.port}" = {};
          };
        };
      };
    };

    "${joinerApp.label}" = {
      metadata.labels.app = joinerApp.label;

      spec = {
        replicas = 1;
        selector.matchLabels.app = joinerApp.label;
        template = {
          metadata.labels.app = joinerApp.label;
          spec.containers."${joinerApp.label}" = {
            name = "${joinerApp.label}";
            image = "joiner-service:latest";
            imagePullPolicy = joinerApp.imagePolicy;
            env = joinerApp.env;
            resources.requests.cpu = joinerApp.cpu;
            ports."${toString joinerApp.port}" = {};
          };
        };
      };
    };
  };

  kubernetes.resources.services = {
    "${helloApp.label}" = {
      spec.selector.app = "${helloApp.label}";
      spec.ports."${toString helloApp.port}".targetPort = helloApp.port;
    };

    "${worldApp.label}" = {
      spec.selector.app = "${worldApp.label}";
      spec.ports."${toString worldApp.port}".targetPort = worldApp.port;
    };

    "${joinerApp.label}" = {
      spec.selector.app = "${joinerApp.label}";
      spec.ports."8080".targetPort = joinerApp.port;
    };
  };

  kubernetes.resources.ingresses = {
    "app-ingress" = {
      spec.rules = [
        {
          host = "localhost";
          http.paths = [
            {
              pathType = "Prefix";
              path = "/";
              backend = let
                svc = kubernetes.resources.services."${joinerApp.label}";
              in {
                serviceName = joinerApp.label;
                servicePort = 8080;
              };
            }
          ];
        }
      ];
    };
  };
}
