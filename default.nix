{ channel ? "stable", pkgs ? import <nixpkgs> {} }:

let
  helloService  = pkgs.callPackage ./service-hello {};
  worldService  = pkgs.callPackage ./service-world {};
  joinerService = pkgs.callPackage ./service-joiner {};
  kubenix       = pkgs.callPackage ./nix/kubenix.nix {};

  buildConfig = t: kubenix.buildResources { configuration = import ./configuration.nix { type = t; }; };

  helloImage = pkgs.dockerTools.buildLayeredImage {
    name = "hello-service";
    tag = "latest";
    config.Cmd = [ "${helloService}/bin/service-hello" ];
  };

  worldImage = pkgs.dockerTools.buildLayeredImage {
    name = "world-service";
    tag = "latest";
    config.Cmd = [ "${worldService}/bin/service-world" ];
  };

  joinerImage = pkgs.dockerTools.buildLayeredImage {
    name = "joiner-service";
    tag = "latest";
    config.Cmd = [ "${joinerService}/bin/service-joiner" ];
  };
in rec {
  inherit helloImage worldImage joinerImage;

  test-deployment = pkgs.callPackage ./nix/test-deployment.nix { };

  deploy-to-kind = pkgs.callPackage ./nix/deploy-to-kind.nix {
    inherit helloImage worldImage joinerImage;

    config = buildConfig "dev";
  };

  config = buildConfig "dev";

  redeploy = pkgs.writeScriptBin "redeploy" ''
    cat ${config} | ${pkgs.kubectl}/bin/kubectl apply -f -
  '';

  deploy-and-test = pkgs.mkShell {
    buildInputs = [ pkgs.kind deploy-to-kind test-deployment ];

    shellHook = ''
      deploy-to-kind
      export KUBECONFIG=$(kind get kubeconfig-path)
      wait-for-deployment hello
      test-deployment
      kind delete cluster
    '';
  };

  shell = let
    moz_overlay = import (
      builtins.fetchTarball https://github.com/mozilla/nixpkgs-mozilla/archive/master.tar.gz
    );

    pkgs = import <nixpkgs> { overlays = [ moz_overlay ]; };
    rustchannel = pkgs.rustChannelOf { inherit channel; };
  in
  pkgs.mkShell {
    buildInputs = with pkgs; [
      kind
      deploy-to-kind
      test-deployment
      redeploy
      curl
      docker
      kubectl

      rustchannel.rust-std
      rustchannel.rust
      rustchannel.rustc
      rustchannel.cargo

      cmake
      curl
      gcc
      openssl
      pkgconfig
    ];

    LIBCLANG_PATH = "${pkgs.llvmPackages.libclang}/lib";
  };

}
