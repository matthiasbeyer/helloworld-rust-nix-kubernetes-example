{ pkgs ? import <nixpkgs> {} }:

with pkgs.stdenv;
with pkgs.lib;

pkgs.rustPlatform.buildRustPackage rec {
  name = "joiner-service";
  src  = pkgs.nix-gitignore.gitignoreSourcePure "target\n" ./.;

  cargoSha256 = "sha256:1dyyhy1c6h0cx2qjpvbns9j7zkbq782k6lgnsxjvvxkcki9zirwa";

  nativeBuildInputs = with pkgs; [ pkg-config makeWrapper ];
  buildInputs       = with pkgs; [ openssl ];
}
