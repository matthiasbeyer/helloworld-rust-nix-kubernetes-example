{ pkgs }:

import (pkgs.fetchFromGitHub {
    owner = "xtruder";
    repo = "kubenix";
    rev = "473a18371d2c0bb22bf060f750589695fb7d3100";
    sha256 = "sha256:0j3mzg81s8pd8gsa3lx7bmhawjpb3f5li2irh55kr7b9kyxr2nsy";
  }) { inherit pkgs; }
