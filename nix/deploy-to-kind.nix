{ kind
, helloImage
, worldImage
, joinerImage
, clusterName ? "cluster"
, config
, pkgs }:

let
  kindConfig = pkgs.writeText "kind-config" ''
    kind: Cluster
    apiVersion: kind.x-k8s.io/v1alpha4
    nodes:
      - role: control-plane
        kubeadmConfigPatches:
          - |
            kind: InitConfiguration
            nodeRegistration:
              kubeletExtraArgs:
                node-labels: "ingress-ready=true"
        extraPortMappings:
          - containerPort: 80
            hostPort: 8080
            protocol: TCP
          - containerPort: 443
            hostPort: 8443
            protocol: TCP
  '';
in

pkgs.writeScriptBin "deploy-to-kind" ''
      #! ${pkgs.runtimeShell}
      set -euo pipefail

      ${kind}/bin/kind delete ${clusterName} || true
      ${kind}/bin/kind create ${clusterName} --config ${kindConfig}

      echo "Loading the ${pkgs.docker}/bin/docker image inside the kind docker container ..."

      kind load image-archive <(gzip --decompress --stdout ${helloImage})
      kind load image-archive <(gzip --decompress --stdout ${worldImage})
      kind load image-archive <(gzip --decompress --stdout ${joinerImage})

      echo "Applying the configuration ..."
      cat ${config} | ${pkgs.jq}/bin/jq "."
      cat ${config} | ${pkgs.kubectl}/bin/kubectl apply -f -

      echo "Applying nginx ingress ..."
      kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/kind/deploy.yaml
''

